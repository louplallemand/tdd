
const addition = (a, b) => {
    if ( typeof(a) !== 'number' || typeof(b) !== 'number' ){
        return 'Error'
    }
    if ( a == null || b == null){
        return 'Error'
    }
    if ( a < 0.00000000000000000001 || b < 0.00000000000000000001){
        let la = Number(a.toString().split('e-')[1])
        let lb = Number(b.toString().split('e-')[1])
        return  la > lb ? (a*(Math.pow(10, la)) + b*(Math.pow(10, la)))/(Math.pow(10, la)) : (a*(Math.pow(10, lb)) + b*(Math.pow(10, lb)))/(Math.pow(10, lb))
    }
    let la = a.toString().split('.').length
    let lb = b.toString().split('.').length
    return  la > lb ? (a*(10*la) + b*(10*la))/(la*10) : (a*(10*lb) + b*(10*lb))/(lb*10)
}

exports.addition = addition;

const multiplication = (a, b) => {
    if ( typeof(a) !== 'number' || typeof(b) !== 'number' ){
        return 'Error'
    }
    if ( a == null || b == null){
        return 'Error'
    }
    let la = a.toString().split('.').length
    let lb = b.toString().split('.').length
    return  la > lb ? (a*(10*la) * b*(10*la))/(la*la*100) : (a*(10*lb) * b*(10*lb))/(lb*lb*100)
}

exports.multiplication = multiplication;