const expect = require('chai').expect;
const {addition} = require('../src/calc');
const {multiplication} = require('../src/calc');

describe("addition", () => {
    it("fonction should return addition of both params", () => {
        expect(addition(3, 5)).to.equal(8);
    });
    it("fonction should return error if no params sent", () => {
        expect(addition()).to.equal('Error');
    });
    it("fonction should return error if only one is sent", () => {
        expect(addition(1)).to.equal('Error');
    });
    it("fonction should return error if a string is sent", () => {
        expect(addition(1, 'a')).to.equal('Error');
    });
    it("fonction should return error if a boolean is sent", () => {
        expect(addition(true, 2)).to.equal('Error');
    });
    it("fonction should return error if a array is sent", () => {
        expect(addition(['a', 1], 2)).to.equal('Error');
    });
    it("fonction should return error if a object is sent", () => {
        expect(addition({a: 1}, 2)).to.equal('Error');
    });
    it("fonction should return small decimal numbers", () => {
        expect(addition(0.1, 0.7)).to.equal(0.8);
    });
    it("fonction should return exact result with both numbers being different length with floating numbers", () => {
        expect(addition(0.000000000000000000001, 0.000000000000000000007)).to.equal(0.000000000000000000008);
    });
})


describe("multiplication", () => {
    it("fonction should return multiplication of both params", () => {
        expect(multiplication(3, 5)).to.equal(15);
    });
    it("fonction should return error if no params sent", () => {
        expect(multiplication()).to.equal('Error');
    });
    it("fonction should return error if only one is sent", () => {
        expect(multiplication(1)).to.equal('Error');
    });
    it("fonction should return error if a string is sent", () => {
        expect(multiplication(1, 'a')).to.equal('Error');
    });
    it("fonction should return error if a boolean is sent", () => {
        expect(multiplication(true, 2)).to.equal('Error');
    });
    it("fonction should return error if a array is sent", () => {
        expect(multiplication(['a', 1], 2)).to.equal('Error');
    });
    it("fonction should return error if a object is sent", () => {
        expect(multiplication({a: 1}, 2)).to.equal('Error');
    });
    it("fonction should return small decimal numbers", () => {
        expect(multiplication(0.1, 0.7)).to.equal(0.07);
    });
    it("fonction should return exact result with both numbers being different length with floating numbers", () => {
        expect(multiplication(0.1, 0.0000000000000000000000000000000000007)).to.equal(0.00000000000000000000000000000000000007);
    });
})